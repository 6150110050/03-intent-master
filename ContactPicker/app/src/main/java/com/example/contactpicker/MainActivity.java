package com.example.contactpicker;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.content.ContentUris;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            // requesting to the user for permission.
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_CONTACTS}, 100);

        } else {
            //if app already has permission this block will execute.
            readContacts();

        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        readContacts();
    }
    // function to read contacts using content resolver
    private void readContacts() {

        final Cursor c = getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null,
                null, null, null);
        String[] from = new String[] { ContactsContract.Contacts.DISPLAY_NAME_PRIMARY };
        int[] to = new int[] { R.id.itemTextView };

        SimpleCursorAdapter adapter = new
                SimpleCursorAdapter(this,R.layout.listitemlayout,c,from,to);
        ListView lv = (ListView)findViewById(R.id.contactListView);
        lv.setAdapter(adapter);

        lv.setOnItemClickListener(new ListView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
                    // Move the cursor to the selected item
                    c.moveToPosition(pos);
                    // Extract the row id.
                    int rowId = c.getInt(c.getColumnIndexOrThrow("_id"));
                    // Construct the result URI.
                    Uri outURI =
                            ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, rowId);
                    Intent outData = new Intent();
                    outData.setData(outURI);
                    setResult(Activity.RESULT_OK, outData);
                    finish();
                }
            });
    }
}